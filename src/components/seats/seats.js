import React from 'react';
import Seat from '../seat/seat';
import './seats.css';
const Seats = ({ tickets }) => {
  return (
    <div className='seats'>
      {tickets.map((seat, index) => {
        return <Seat detail={seat} key={index} seatNo={index + 1} />;
      })}
    </div>
  );
};

export default Seats;
