import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectTicket, unselectTicket } from '../../store/actions/index';
import './seat.css';
const Seat = ({ detail, seatNo }) => {
  const selected = useSelector((state) => state.bus.seat_no);
  const dispatch = useDispatch();

  const style_ = () => {
    let style = 'seat';
    if (detail.isAvail === false) style = style + ' seat__booked';
    if (seatNo === selected) style = style + ' seat__selected';
    // console.log(style);
    return style;
  };

  const selectHandler = () => {
    if (seatNo == -1 || detail.isAvail == false) return;
    if (seatNo == selected) dispatch(unselectTicket());
    else dispatch(selectTicket(seatNo));
  };

  return (
    <div onClick={selectHandler} className={style_()}>
      <p>{seatNo == -1 ? null : seatNo}</p>
    </div>
  );
};

export default Seat;
