import { useEffect } from 'react';
import { createPortal } from 'react-dom';
import { useDispatch } from 'react-redux';
import { closeModal } from '../../store/actions';
import './portal.css';

const Portal = ({ children, size = 'small' }) => {
  const dispatch = useDispatch();
  const mount = document.getElementById('portal-root');
  const el = document.createElement('div');
  el.className = 'portal';

  const nest = document.createElement('div');

  nest.className = `portal__inner portal__${size}`;
  const backdrop = document.createElement('div');
  backdrop.className = 'portal__backdrop';
  backdrop.addEventListener('click', () => {
    console.log('triggered');
    return dispatch(closeModal());
  });

  el.appendChild(backdrop);
  el.appendChild(nest);

  useEffect(() => {
    mount.appendChild(el);

    return () => {
      el.removeEventListener('click', () => {
        console.log('EL removed');
      });
      mount.removeEventListener('click', () => {
        console.log('EL removed');
      });
      mount.removeChild(el);
    };
  }, [el, mount]);

  return createPortal(children, nest);
};

export default Portal;
