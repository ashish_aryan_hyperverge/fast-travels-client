import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { closeModal } from '../../store/actions/index';
import Portal from '../portal/portal';
import Busform from '../busform/busform';
import Login from '../login/login';
const Modal = () => {
  const { open, type, size } = useSelector((state) => state.modal);
  const dispatch = useDispatch();
  let Inner = null;
  if (!open) return null;
  switch (type) {
    case 'Busform':
      Inner = Busform;
      break;
    case 'Login':
      Inner = Login;
      break;
    default:
      Inner = null;
  }
  return (
    <div className='modal'>
      <Portal size={size}>
        <Inner />
      </Portal>
    </div>
  );
};

export default Modal;
