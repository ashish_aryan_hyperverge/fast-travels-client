import React, { useState } from 'react';
import Button from '../button/button';
import { postNewBus, closeModal } from '../../store/actions/index';
import { useDispatch } from 'react-redux';
import './busform.css';
const Busform = () => {
  const [form, setForm] = useState({});
  const dispatch = useDispatch();
  const formHandler = (e) => {
    console.log(form);
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const formSubmitHandler = () => {
    console.log('clicked');
    dispatch(closeModal());
    dispatch(postNewBus(form));
  };

  return (
    <div className='busform'>
      <p>Make entry for a new bus</p>
      <form>
        <input placeholder='Name' name='name' onChange={formHandler} required />
        <input
          placeholder='Source'
          name='source'
          onChange={formHandler}
          required
        />
        <input
          placeholder='Destination'
          name='dest'
          onChange={formHandler}
          required
        />
        <input placeholder='cost' name='cost' onChange={formHandler} required />

        <input
          placeholder='Bus Number'
          name='bus_no'
          onChange={formHandler}
          required
        />
        <input
          placeholder='Date'
          name='date'
          type='date'
          onChange={formHandler}
        />
        <input
          placeholder='boarding time'
          name='boarding_time'
          type='time'
          onChange={formHandler}
        />
        <input
          placeholder='dropping time'
          name='dropping_time'
          type='time'
          onChange={formHandler}
        />
      </form>
      <Button value='CREATE' color='success' onClick={formSubmitHandler} />
    </div>
  );
};

export default Busform;
