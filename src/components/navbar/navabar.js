import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { openModal } from '../../store/actions/index';
import Button from '../button/button';
import './navbar.css';
function Navbar(props) {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user.user);
  return (
    <div className='navbar'>
      <div className='navbar__inner'>
        <div className='navbar__left'>Trip-O-Trip</div>
        <div className='navbar__right'>
          <div>Tickets</div>
          <div>Offers</div>
          <div>Cancellation</div>
          {!user && (
            <div
              onClick={() =>
                dispatch(openModal({ type: 'Login', size: 'small' }))
              }>
              Admin
            </div>
          )}
          {user && user.isAdmin && (
            <>
              <div
                onClick={() =>
                  dispatch(openModal({ type: 'Busform', size: 'large' }))
                }>
                Add Buses
              </div>
              <Button value='logout' color='danger' />
            </>
          )}
        </div>
      </div>
    </div>
  );
}

export default Navbar;
