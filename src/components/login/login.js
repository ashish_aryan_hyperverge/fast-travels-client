import React, { useState } from 'react';
import Button from '../button/button';
import { useDispatch } from 'react-redux';
import { closeModal, userLogin } from '../../store/actions/index';
import './login.css';
function Login(props) {
  const [login, setLogin] = useState({});
  const dispatch = useDispatch();
  const changeHandler = (e) => {
    setLogin({
      ...login,
      [e.target.name]: e.target.value,
    });
  };

  const submitHandler = () => {
    dispatch(userLogin(login));
    dispatch(closeModal());
  };
  return (
    <div className="form-login">
      <p>Admin Login panel</p>
      <div className="input-field-login">

        <label >Mobile Number</label>
        <input type="text" placeholder="9876543210" name="phone" value={login.phone}
          onChange={changeHandler} />
      </div>
      <div className="input-field-login">
        <label htmlFor="password">Password</label>
        <input type="password" placeholder="********" name="password" value={login.password}
          onChange={changeHandler} />
      </div>

      <Button value='Login' color='danger' onClick={submitHandler} />
    </div>

  );
}

export default Login;
