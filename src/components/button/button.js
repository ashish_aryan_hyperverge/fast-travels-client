import React from 'react';
import './button.css';
function Button(props) {
  return (
    <div className={'customButton ' + props.color} onClick={props.onClick}>
      <div className='customButton__text'>{props.value}</div>
    </div>
  );
}

export default Button;
