import React from 'react';
import Button from '../button/button';
import { useDispatch, useSelector } from 'react-redux';
import { addCurrentBus } from '../../store/actions/index';
import { useHistory } from 'react-router-dom';
import logo from '../../assets/images/traveller__logo_2.png';

import './busTile.css';
function BusTile({ data }) {
  const history = useHistory();
  const dispatch = useDispatch();
  const bookHandler = () => {
    dispatch(addCurrentBus(data));
    history.push('/booking');
  };
  return (
    <div className='bustile'>
      <div className='bustile__inner'>
        <div className='bustile__header'>
          <h3>{data.name}</h3>
          <p>A/c seater</p>
        </div>

        <div className='bustile__departure'>
          <h4>{data.boarding_time}</h4>
        </div>

        <div className='bustile__date'>
          <h5>{data.date}</h5>
        </div>
        <div className='bustile__arrival'>
          <h4>{data.dropping_time}</h4>
        </div>

        <div className='bustile__price'>
          <h4>{data.cost}/-</h4>
          <p>Seats left: {data.availability}</p>
        </div>
        <div className='bustile__book'>
          <Button value='Book' color={'danger'} onClick={bookHandler} />
        </div>
      </div>
    </div>
  );
}

export default BusTile;
