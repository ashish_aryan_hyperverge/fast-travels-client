import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '../button/button';
import './searchTile.css';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { fetchAllBus } from '../../store/actions';
function SearchTile(props) {
  const dispatch = useDispatch();
  const [form, setForm] = useState({});
  const formHandler = (e) => {
    console.log(e.target.value);
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    })
  };
  const submitHandler = () => {
    dispatch(fetchAllBus(form));
  };

  return (
    <div className='searchTile'>
      <div className='searchTile__inner'>
        <div className='searchTile__input'>
          <TextField
            id='filled-basic'
            label='From'
            variant='standard'
            size='medium'
            name="sourceCity"
            value={form.sourceCity}
            onChange={formHandler}
          />
          <div className='searchTile__input__center'>&#8652;</div>
          <TextField
            id='filled-basic'
            label='to'
            variant='standard'
            size='medium'
            name="destinationCity"
            value={form.destinationCity}
            onChange={formHandler}
          />
          <TextField
            id='date'
            onChange={formHandler}
            value={form.travelDate}
            name="travelDate"
            label='Date'
            type='date'
            defaultValue='2021-02-02'
            size='medium'
            InputLabelProps={{
              shrink: true,
            }}
          />
          <Button
            value={'Search'}
            color={'danger'}
            width='100px'
            height='30px'
            onClick={submitHandler}
          />
        </div>
      </div>
    </div>
  );
}

export default SearchTile;
