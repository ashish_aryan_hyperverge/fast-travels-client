import React, { useEffect } from 'react';
import { fetchAllBus } from '../../store/actions/index';
import { useDispatch, useSelector } from 'react-redux';
import BusTile from '../../components/busTile/busTile';
import SearchTile from '../../components/searchTile/searchTile';
import './landingPage.css';
function LandingPaage(props) {
  const dispatch = useDispatch();
  const buses = useSelector((state) => state.bus);


  return (
    <div className='landing'>
      <div className='landing__center'>
        <div className='landing__search'>
          <SearchTile />
        </div>
        <div className='landing__header'>
          <p>Operator</p>
          <p>Departure</p>
          <p>Date</p>
          <p>Arrival</p>
          <p>Price</p>
          <p>Select Seat</p>
        </div>
        {buses.bus.map((bus, index) => {
          return <BusTile key={index} data={bus} />;
        })}
      </div>
    </div>
  );
}

export default LandingPaage;
