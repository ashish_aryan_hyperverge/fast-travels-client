import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { bookTicket, resetTicket } from '../../store/actions/index';
import { Redirect } from 'react-router-dom';
import Button from '../../components/button/button';
import Seat from '../../components/seat/seat';
import Seats from '../../components/seats/seats';
import Textbox from '@material-ui/core/TextField';
import './booking.css';
const Booking = () => {
  const state = useSelector((state) => state.bus.current_bus);
  const selected = useSelector((state) => state.bus.seat_no);
  const user = useSelector((state) => state.user.user);
  const dispatch = useDispatch();
  const [booking, setBooking] = useState({});
  const bookingHandler = () => {
    if (selected == -1) return;
    let body = booking;
    body.ticket_no = selected;
    body.bus_id = state._id;
    dispatch(bookTicket(body));
    setBooking({});
  };

  const onChangeHandler = (e) => {
    setBooking({
      ...booking,
      [e.target.name]: e.target.value,
    });
    console.log(booking);
  };

  const ticketResetHandler = () => {
    dispatch(resetTicket(state._id));
  };

  if (state == null) return <Redirect to='/' />;
  return (
    <div className='booking'>
      <div className='booking__header'>
        <div>
          <p>
            <strong>{state.name}</strong>
            <p>A/c seater</p>
          </p>
          <p>
            {state.cost}/-
            <p>cost</p>
          </p>
          <p>
            {state.availability}
            <p>Seats left</p>
          </p>
          <p>
            {state.source}
            <p>Source</p>
          </p>
          <p>
            {state.dest}
            <p>Destination</p>
          </p>
          <p>
            {state.date}
            <p>Date</p>
          </p>
          <p>
            {state.boarding_time}
            <p>Departure</p>
          </p>
          <p>
            {state.dropping_time}
            <p>Arrival</p>
          </p>
        </div>
      </div>
      <div className='booking__inner'>
        {/* <div className='booking__inner__header'>
          <p>Seat Layout</p>
        </div> */}
        <div className='booking__layout'>
          <p className='booking__layout__info'>
            Click on the available seat to proceed with the booking.
          </p>
          <div className='booking__layout__inner'>
            <div className='booking__left'>
              <div>
                <Seat detail={{ isAvail: false }} seatNo='-1' />
                <p>booked</p>
                <Seat detail={{ isAvail: true }} seatNo='-1' />
                <p>Available</p>
              </div>
            </div>
            <div className='booking__seats'>
              <Seats tickets={state.tickets} />
              <p>
                <br />
                <strong>Booked Seats:</strong> {selected == 0 ? 0 : 1}
              </p>
              {user && (
                <Button
                  value='Reset'
                  color='danger'
                  onClick={ticketResetHandler}
                />
              )}
            </div>
          </div>

          {selected != 0 && (
            <div className='booking__right'>
              <p>Booking Form [seat: {selected}]</p>
              <form>
                <input
                  placeholder='Phone'
                  name='phone'
                  onChange={onChangeHandler}
                  value={booking.phone}
                />
                <input
                  placeholder='Name'
                  className='booking__right__name'
                  name='name'
                  onChange={onChangeHandler}
                  value={booking.name}
                />
                <input
                  placeholder='Age'
                  className='booking__right__age'
                  name='age'
                  onChange={onChangeHandler}
                  value={booking.age}
                />
              </form>
              <Button value='Book' color='danger' onClick={bookingHandler} />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Booking;
