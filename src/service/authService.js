
import axios from '../utils/axios';

const userLogin = (data) => {
    return axios.post('/api/user/login', data);
};

const fetchUser = () => {
    return axios.get('/api/');
};


export default {
    userLogin,
    fetchUser,
};
