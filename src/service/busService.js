
import axios from '../utils/axios';
import authHeader from "./authHeader";


const fetchAllBus = (data) => {
    console.log(data)
    return axios
        .post('/api/bus/all', data);
};

const postNewBus = (data) => {
    return axios
        .post('/api/bus/addbus', data, { headers: authHeader() });
};
const bookTicket = (data) => {
    return axios
        .post('/api/ticket/booking', data);
}
const resetTicket = (bus_id) => {
    return axios
        .get('/api/bus/reset/' + bus_id, { headers: authHeader() })
}

export default {
    fetchAllBus,
    postNewBus,
    bookTicket,
    resetTicket
};
