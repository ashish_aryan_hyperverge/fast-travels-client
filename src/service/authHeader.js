export default function authHeader() {
    const user = localStorage.getItem("token");
    console.log(user)
    if (user) {
        return { "x-access-token": user };
    } else {
        return {};
    }
}