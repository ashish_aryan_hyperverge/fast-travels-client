import { useDispatch, useSelector } from 'react-redux';
import { increment } from './store/actions/busAction';
import Navbar from './components/navbar/navabar';
import Booking from './containers/booking/booking';
import LandingPage from './containers/landingPage/landingPage';
import Modal from './components/modal/modal';
import { Switch, Route } from 'react-router-dom';
import './App.css';

function App() {
  const count = useSelector((state) => {
    return state.bus.count;
  });
  const dispatch = useDispatch();

  return (
    <div className='App'>
      <Modal />
      <Navbar />
      <div className='App__body'>
        <Switch>
          <Route exact path='/' render={() => <LandingPage />} />
          <Route path='/booking' render={() => <Booking />} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
