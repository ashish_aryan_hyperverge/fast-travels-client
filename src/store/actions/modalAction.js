export const openModal = (data) => {
  return {
    type: 'OPEN',
    payload: data,
  };
};

export const closeModal = () => {
  return {
    type: 'CLOSE',
  };
};
