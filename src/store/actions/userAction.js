import authService from "../../service/authService";

export const addUser = (user) => {
  return {
    type: 'LOGIN',
    payload: user,
  };
};

export const addUserError = (err) => {
  return {
    type: 'USER_ERROR',
    payload: err,
  };
};

export const signout = () => {
  return {
    type: 'SIGNOUT',
  };
};

export const fetchUser = () => {
  return (dispatch) => {
    return authService.fetchUser()
      .then((response) => {
        dispatch(addUser(response));
      })
      .catch((err) => {
        dispatch(addUserError(err));
      });
  };
};
export const userLogin = (data) => {
  return (dispatch) => {
    authService.userLogin(data)
      .then((response) => {
        if (response.data.token) {
          localStorage.setItem("token", response.data.token);
        }
        dispatch(addUser(response.data.user));
      })
      .catch((err) => {
        dispatch(addUserError(err));
      });
  };
};
