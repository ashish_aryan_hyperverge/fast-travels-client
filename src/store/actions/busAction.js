import busService from '../../service/busService';

export const addBuses = (bus) => {
  return {
    type: 'BUS_UPDATE',
    payload: bus,
  };
};

export const addBusError = (err) => {
  return {
    type: 'BUS_ERROR',
    payload: err,
  };
};

export const fetchAllBus = (data) => {
  return (dispatch) => {
    busService.fetchAllBus(data)
      .then((response) => {
        console.log('fetch bus', response.data);
        dispatch(addBuses(response.data));
      })
      .catch((err) => {
        console.log(err);
        dispatch(addBusError(err));
      });
  };
};

export const postNewBus = (data) => {
  console.log('inside postNewBus');
  return (dispatch) => {
    busService.postNewBus(data)
      .then((response) => {
        console.log('reponse postnew bus ', data);
      })
      .catch((err) => {
        console.log(err);
        dispatch(addBusError(err));
      });
  };
};
export const addCurrentBus = (data) => {
  return {
    type: 'ADD_CURRENT_BUS',
    payload: data,
  };
};
export const selectTicket = (data) => {
  return {
    type: 'SELECT_TICKET',
    payload: data,
  };
};

export const unselectTicket = () => {
  return {
    type: 'UNSELECT',
  };
};

export const bookTicket = (data) => {
  return (dispatch) => {
    busService.bookTicket(data)
      .then((response) => {
        console.log(response.data);
        dispatch(addCurrentBus(response.data));
      })
      .catch((err) => {
        dispatch(addBusError(err));
      });
  };
};

export const resetTicket = (bus_id) => {
  return (dispatch) => {
    busService.resetTicket(bus_id)
      .then((response) => {
        console.log(response.data);
        dispatch(addCurrentBus(response.data));
      })
      .catch((err) => {
        console.log(err);
        dispatch(addBusError(err));
      });
  };
};
