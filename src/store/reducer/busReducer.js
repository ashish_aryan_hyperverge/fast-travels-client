const initialState = {
  bus: [],
  current_bus: null,
  seat_no: 0,
  err: null,
};

const busReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'BUS_UPDATE':
      return {
        ...state,
        bus: action.payload,
      };
    case 'BUS_ERROR':
      return {
        ...state,
        bus: [],
        current_bus: null,
        err: action.payload,
        seat_no: 0,
      };
    case 'ADD_CURRENT_BUS':
      return {
        ...state,
        current_bus: action.payload,
        seat_no: 0,
      };
    case 'SELECT_TICKET':
      return {
        ...state,
        seat_no: action.payload,
      };
    case 'UNSELECT':
      return {
        ...state,
        seat_no: 0,
      };
    default:
      return state;
  }
};

export default busReducer;
