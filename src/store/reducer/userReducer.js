const initialState = {
  user: null,
  err: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        user: action.payload,
      };
    case 'SIGNOUT':
      return {
        ...state,
        user: null,
      };
    case 'USER_ERROR':
      return {
        user: null,
        err: action.payload,
      };
    default:
      return state;
  }
};

export default userReducer;
