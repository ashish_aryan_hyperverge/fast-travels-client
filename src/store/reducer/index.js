import { combineReducers, applyMiddleware, createStore } from 'redux';
// import { }  from 'react-redux';
import busReducer from './busReducer';
import modalReducer from './modalReducer';
import userReducer from './userReducer';
import thunk from 'redux-thunk';
const rootReducer = combineReducers({
  bus: busReducer,
  modal: modalReducer,
  user: userReducer,
});
const store = createStore(rootReducer, applyMiddleware(thunk));
export default store;
