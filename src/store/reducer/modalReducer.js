const initialState = {
  open: false,
  type: '',
  size: 'small',
};

const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'OPEN':
      return {
        open: true,
        type: action.payload.type,
        size: action.payload.size,
      };
    case 'CLOSE':
      return {
        open: false,
        type: '',
        size: 'small',
      };
    default:
      return state;
  }
};

export default modalReducer;
